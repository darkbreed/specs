Pod::Spec.new do |s|
  s.name         = "BRMediaPicker"
  s.version      = "0.0.1"
  s.summary      = "Library for picking multiple media items from the devices media galleries. A wrapper lib for ALAssetManager etc."
  s.homepage     = "http://github.com/darkbreed/BRMediaPicker"
  s.license      = 'MIT'
  s.author       = { "Ben Reed" => "ben@benreed.me" }
  s.source       = { :git => "http://github.com/darkbreed/BRMediaPicker.git", :branch => "master" }
  s.platform     = :ios, '5.1'
  s.source_files = 'BRMediaPicker', '*.{h,m}'
  s.requires_arc = true
end

